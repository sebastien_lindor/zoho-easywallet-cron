<?php 
use zcrmsdk\crm\setup\restclient\ZCRMRestClient;
use zcrmsdk\crm\crud\ZCRMModule;
require_once __DIR__ . '/vendor/autoload.php';
include __DIR__ . '/vendor/database.php';

class ZohoClient{

	private function json_decode($json, $assoc = TRUE){
		$json = str_replace(array("\n","\r"),"\\n",$json);
		$json = preg_replace('/([{,]+)(\s*)([^"]+?)\s*:/','$1"$3":',$json);
		$json = preg_replace('/(,)\s*}$/','}',$json);
		return json_decode($json,$assoc);
	}

	public function getAccessTokenWithRefreshToken($refresh_token){
		$data = array(
		'refresh_token' => $refresh_token,
		'client_id' => '1000.PTAQGWKUAV1RVXX7AQZRBRKPJRZGFR',
		'client_secret' => '4f69ebe27d54ca83cf8ad5e19aedd27cc74df81973',
		'grant_type' => 'refresh_token'
		);
		return $this->getCurlReponse($data);

	}

	
	public function getRefreshTokenFromGrantToken($grant_token){
		$data = array(
		'code' => $grant_token,
		'client_id' => '1000.PTAQGWKUAV1RVXX7AQZRBRKPJRZGFR',
		'client_secret' => '4f69ebe27d54ca83cf8ad5e19aedd27cc74df81973',
		'grant_type' => 'authorization_code'
		);
		return $this->getCurlReponse($data);

	}

	public function getCurlReponse($data){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://accounts.zoho.eu/oauth/v2/token",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS     => $data ,  // this are my post vars
		));
		$response = curl_exec($curl);
		curl_close($curl);

		$response = json_decode($response);
		return $response;
	}
}

$data = new ZohoClient();
$refresh_token = '1000.c4ef0a8076ea8310e9f1873ef66e2fc4.60e9f31e253f18d0fef3448053df05d0';
$access_token_set = $data->getAccessTokenWithRefreshToken($refresh_token);

$access_token = $access_token_set->access_token;
$expiry_time = time() + (60 * 60);
var_dump($access_token);
var_dump($expiry_time);

$data = new Database();
$data = $data->addTokens($access_token, $refresh_token);

