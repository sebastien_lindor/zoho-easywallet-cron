<?php  

class Database{

	private function setDatabaseConfig(){
		$active_group = 'default';
		$active_record = TRUE;

		//DEFAULT
		$db['default']['hostname'] = 'localhost';
		$db['default']['username'] = 'root';
		$db['default']['password'] = '';
		$db['default']['database'] = 'crontest';
		$db['default']['dbdriver'] = 'mysqli';
		$db['default']['dbprefix'] = '';
		$db['default']['pconnect'] = FALSE;
		$db['default']['db_debug'] = TRUE;
		$db['default']['cache_on'] = FALSE;
		$db['default']['cachedir'] = '';
		$db['default']['char_set'] = 'utf8';
		$db['default']['dbcollat'] = 'utf8_general_ci';
		$db['default']['swap_pre'] = '';
		$db['default']['autoinit'] = TRUE;
		$db['default']['stricton'] = FALSE;
		return $db;
	}

	public function openCon()
	 {
		$db = $this->setDatabaseConfig();
		 $conn = new mysqli($db['default']['hostname'], $db['default']['username'], $db['default']['password'], $db['default']['database']) or die("Connect failed: %s\n". $conn -> error);
		 
		 return $conn;
	 }

	 public function addTokens($access_token, $refresh_token)
	{
	    
	    $req = new Database();
		$req = $req->OpenCon();
	    
	    $sql = "INSERT INTO `tokens`(`access_token`, `refresh_token`) VALUES ('" . $access_token . "','" . $refresh_token . "')";
	    if ($req->query($sql) === TRUE) {
		  echo "Tokens been added sucessfully ";
		} else {
		  echo "Error adding tokens: " . $req->error;
		}
	     mysqli_close($req);
	}

	 public function updateAccess_Token($access_token, $refresh_token)
	{
	    
	    $req = new Database();
		$req = $req->OpenCon();
	    
	    $sql = "UPDATE `tokens` SET access_token=$access_token WHERE refresh_token=$refresh_token";
	     if ($req->query($sql) === TRUE) {
		  	echo "Tokens updated successfully";
		} else {
		  	echo "Error updating tokens: " . $req->error;
		}
		mysqli_close($req);
		
	}
 
	
}   

